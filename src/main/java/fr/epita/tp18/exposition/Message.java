package fr.epita.tp18.exposition;

import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.jms.core.MessageCreator;

public class Message implements MessageCreator{
	String message;
	
	public Message(String message) {
		this.message=message;
	}

	@Override
	public javax.jms.Message createMessage(Session session) throws JMSException {
		TextMessage tm=session.createTextMessage(message);
	
		return tm;
	}

}

package fr.epita.tp18.exposition;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class SendController {
	
	@Autowired
	JmsTemplate jmsTemplate;
	
	@GetMapping("/send/{message}")
	public  void sendMessage(@PathVariable("message") String message) {
		
		
		jmsTemplate.send("Queue_epita",new fr.epita.tp18.exposition.Message(message));
		
	}

}
